import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class TareaService {
  tareaList:Array<any> = [];
  uriTarea = "http://localhost:3000/api/v1/tarea/";

  constructor(private _http:Http) {}

  getUsuarios() {
  return this._http.get(this.uriTarea).map(res => {
      this.tareaList= res.json();
    });
  }
}