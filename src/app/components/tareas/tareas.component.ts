import { Component, OnInit } from '@angular/core';
import { TareaService } from '../../services/tarea.service';

@Component({
  selector: 'app-tareas',
  templateUrl: './tareas.component.html',
})

export class tareaComponent implements OnInit {
  usuarios:any[];
  constructor(public _userS:TareaService) {
  }
  ngOnInit() {
    this._userS.getUsuarios().subscribe();
  }
}
